function worker(gen, ...arg) {
  const iterator = gen(...arg);
  let finalResult = null;

  function recursion(result) {
    if (result.value instanceof Promise) {
      result.value.then(res => {
        recursion(iterator.next(res));
      });
    } 

    if (typeof result.value === 'function') {
      try {
        return recursion(iterator.next(result.value()));
      } catch {
        return recursion(iterator.throw(result.value));
      }
    } 
    
     if (result.done) {
      return finalResult(result.value);
    } 
    recursion(iterator.next(result.value));
  }
  recursion(iterator.next());

  return new Promise(res => {
    finalResult = res;
  });
}

worker(z, 3, 3).then(console.log);
